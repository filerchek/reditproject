<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\Router;

class ExceptionListener
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        if ($exception->getCode() == 403
            || ($exception->getPrevious() && $exception->getPrevious()->getCode() == 403)) {
            $event->setResponse(new RedirectResponse(
                $this->router->generate('login', [
                    'afterDenied' => 'afterDenied'
                ])
            ));
        }
    }
}
